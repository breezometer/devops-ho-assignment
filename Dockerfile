FROM python:3.8-slim

WORKDIR /proj

RUN pip install flask

COPY . $WORKDIR

CMD python /proj/app.py